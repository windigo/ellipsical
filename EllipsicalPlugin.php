<?php

/**
 * Ellipsical Plugin - GNU Social
 * Copyright (C) 2015, Jacob Hume
 *
 * PHP version 5
 *
 * LICENCE:
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @category  Plugin
 * @package   GNU Social
 * @author    Windigo (Jacob Hume)
 * @copyright 2015 Jacob Hume
 * @license   http://www.fsf.org/licensing/licenses/agpl-3.0.html AGPL 3.0
 */

if (!defined('GNUSOCIAL') && !defined('STATUSNET')) {
    // This check helps protect against security problems;
    // your code file can't be executed directly from the web.
    exit(1);
}


class EllipsicalPlugin extends Plugin {

    function initialize() {
        return true;
    }

    function cleanup() {
        return true;
    }

    function onStartNoticeSave($notice) {

		$notice->rendered = str_replace('...', '…', $notice->rendered);

		return true;
    }

    function onPluginVersion(array &$versions)
    {
        $versions[] = array('name' => 'Ellipsical',
                            'version' => '0.1.1',
                            'author' => 'Windigo (Jacob Hume)',
                            'homepage' => 'https://fragdev.com',
                            'rawdescription' =>
                            _m('Converts any and all three-period sequences to an ellipsis character.'));
        return true;
    }
}

