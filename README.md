Ellipsical Plugin
===

This plugin replaces any sequence of three period characters [...] into an
ellipsis character […].

If you post only dots in your messages, this can result in a 300% savings in
database size.
